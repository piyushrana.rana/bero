import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { VisualizationComponent } from './visualization/visualization.component';
import { RouterModule, Routes } from '@angular/router';
import { from } from 'rxjs';
import { HomeComponent } from './home/home.component';



const appRoutes: Routes = [
 
  { path: 'visual',      component: VisualizationComponent },
  { path: '', component: HomeComponent }


  
];






@NgModule({
  declarations: [
    AppComponent,
    VisualizationComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(
      appRoutes
 
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
